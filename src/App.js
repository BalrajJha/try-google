import logo from './logo.svg';
import './App.css';
import { GoogleOAuthProvider, GoogleLogin } from '@react-oauth/google';
// client id
// 819446456273-jkdgqnphgi9qlqted4lhqd0riit9qfkd.apps.googleusercontent.com
// client secret
// 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
function App() {
  return (
    <GoogleOAuthProvider clientId="819446456273-jkdgqnphgi9qlqted4lhqd0riit9qfkd.apps.googleusercontent.com">
      <GoogleLogin
        onSuccess={credentialResponse => {
          console.log(credentialResponse);
        }}
        onError={() => {
          console.log('Login Failed');
        }}
        cookiePolicy={"single_host_origin"}
      />
    </GoogleOAuthProvider>
  );
}

export default App;
